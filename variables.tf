variable "aws_region" {
  default = "eu-central-1"
}

variable "api_name" {
  default = "api_lambda"
}

variable "function_1_name" {
  default = "function_1"
}

variable "function_2_name" {
  default = "function_2"
}

variable "function_3_name" {
  default = "function_3"
}

variable "function_1_zip_name" {
  default = "lambda_function_1"
}

variable "function_2_zip_name" {
  default = "lambda_function_2"
}

variable "function_3_zip_name" {
  default = "lambda_function_3"
}

variable "function_1_source_file" {
  default = "function_1.js"
}

variable "function_2_source_file" {
  default = "function_2.js"
}

variable "function_3_source_file" {
  default = "function_3.js"
}

variable "function_1_zip_output_path" {
  default = "lambda_function_1.zip"
}

variable "function_2_zip_output_path" {
  default = "lambda_function_2.zip"
}

variable "function_3_zip_output_path" {
  default = "lambda_function_3.zip"
}

variable "archive_type" {
  default = "zip"
}

variable "function_1_handler" {
  default = "index1.handler"
}

variable "function_2_handler" {
  default = "index2.handler"
}

variable "function_3_handler" {
  default = "index3.handler"
}

variable "functions_runtime" {
  default = "nodejs16.x"
}

variable "iam_name" {
  default = "iam_for_lambdas"
}

variable "api_protocol_type" {
  default = "HTTP"
}

variable "api_description" {
  default = "Best api ever"
}

variable "api_stage_name" {
  default = "$default"
}

variable "function_1_integration_name" {
  default = "function_1_integration"
}

variable "function_2_integration_name" {
  default = "function_2_integration"
}

variable "function_3_integration_name" {
  default = "function_3_integration"
}

variable "functions_integration_type" {
  default = "AWS_PROXY"
}
variable "functions_integration_method" {
  default = "POST"
}

variable "function_f1_route_name" {
  default = "api_route_f1"
}

variable "function_f2_route_name" {
  default = "api_route_f2"
}

variable "function_f3_route_name" {
  default = "api_route_f3"
}

variable "f1_route_key" {
  default = "ANY /function_1"
}

variable "f2_route_key" {
  default = "ANY /function_2"
}

variable "f3_route_key" {
  default = "ANY /function_3"
}

variable "lambda_perm_statement_id" {
  default = "AllowExecutionFromAPIGateway"
}

variable "lambda_perm_action" {
  default = "lambda:InvokeFunction"
}

variable "lambda_perm_principal" {
  default = "apigateway.amazonaws.com"
}