provider "aws" {
  region = var.aws_region
}

data "archive_file" "zip_1" {
  type        = var.archive_type
  source_file = var.function_1_source_file
  output_path = var.function_1_zip_output_path
}

data "archive_file" "zip_2" {
  type        = var.archive_type
  source_file = var.function_2_source_file
  output_path = var.function_2_zip_output_path
}

data "archive_file" "zip_3" {
  type        = var.archive_type
  source_file = var.function_3_source_file
  output_path = var.function_3_zip_output_path
}

resource "aws_lambda_function" "function_1" {
  filename         = var.function_1_zip_output_path
  function_name    = var.function_1_name
  role             = aws_iam_role.iam_for_lambdas.arn
  handler          = var.function_1_handler
  source_code_hash = data.archive_file.zip_1.output_base64sha256
  runtime          = var.functions_runtime
}

resource "aws_lambda_function" "function_2" {
  filename         = var.function_2_zip_output_path
  function_name    = var.function_2_name
  role             = aws_iam_role.iam_for_lambdas.arn
  handler          = var.function_2_handler
  source_code_hash = data.archive_file.zip_2.output_base64sha256
  runtime          = var.functions_runtime
}

resource "aws_lambda_function" "function_3" {
  filename         = var.function_3_zip_output_path
  function_name    = var.function_3_name
  role             = aws_iam_role.iam_for_lambdas.arn
  handler          = var.function_3_handler
  source_code_hash = data.archive_file.zip_3.output_base64sha256
  runtime          = var.functions_runtime
}

resource "aws_iam_role" "iam_for_lambdas" {
  name               = var.iam_name
  assume_role_policy = file("${path.module}/iam_policy.json")
}

resource "aws_apigatewayv2_api" "lambda_api" {
  name          = var.api_name
  protocol_type = var.api_protocol_type
  description   = var.api_description

  cors_configuration {
    allow_credentials = false
    allow_headers     = []
    allow_methods = [
      "GET",
      "HEAD",
      "OPTIONS",
      "POST",
      "DELETE",
      "PUT",
    ]
    allow_origins = [
      "*",
    ]
    expose_headers = []
    max_age        = 0
  }
}


resource "aws_apigatewayv2_stage" "default" {
  api_id = aws_apigatewayv2_api.lambda_api.id

  name        = var.api_stage_name
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.api_lambda_cloudwatch.arn

    format = jsonencode({
      requestId               = "$context.requestId"
      sourceIp                = "$context.identity.sourceIp"
      requestTime             = "$context.requestTime"
      protocol                = "$context.protocol"
      httpMethod              = "$context.httpMethod"
      resourcePath            = "$context.resourcePath"
      routeKey                = "$context.routeKey"
      status                  = "$context.status"
      responseLength          = "$context.responseLength"
      integrationErrorMessage = "$context.integrationErrorMessage"
      }
    )
  }
  depends_on = [aws_cloudwatch_log_group.api_lambda_cloudwatch]
}

resource "aws_apigatewayv2_integration" "function_1" {
  api_id             = aws_apigatewayv2_api.lambda_api.id
  description        = var.function_1_integration_name
  integration_uri    = aws_lambda_function.function_1.invoke_arn
  integration_type   = var.functions_integration_type
  integration_method = var.functions_integration_method
}

resource "aws_apigatewayv2_integration" "function_2" {
  api_id             = aws_apigatewayv2_api.lambda_api.id
  description        = var.function_2_integration_name
  integration_uri    = aws_lambda_function.function_2.invoke_arn
  integration_type   = var.functions_integration_type
  integration_method = var.functions_integration_method
}

resource "aws_apigatewayv2_integration" "function_3" {
  api_id             = aws_apigatewayv2_api.lambda_api.id
  description        = var.function_3_integration_name
  integration_uri    = aws_lambda_function.function_3.invoke_arn
  integration_type   = var.functions_integration_type
  integration_method = var.functions_integration_method
}

resource "aws_apigatewayv2_route" "route_f1" {
  api_id         = aws_apigatewayv2_api.lambda_api.id
  operation_name = var.function_f1_route_name
  route_key      = var.f1_route_key
  target         = "integrations/${aws_apigatewayv2_integration.function_1.id}"
}

resource "aws_apigatewayv2_route" "route_f2" {
  api_id         = aws_apigatewayv2_api.lambda_api.id
  operation_name = var.function_f2_route_name
  route_key      = var.f2_route_key
  target         = "integrations/${aws_apigatewayv2_integration.function_2.id}"
}

resource "aws_apigatewayv2_route" "route_f3" {
  api_id         = aws_apigatewayv2_api.lambda_api.id
  operation_name = var.function_f3_route_name
  route_key      = var.f3_route_key
  target         = "integrations/${aws_apigatewayv2_integration.function_3.id}"
}


resource "aws_cloudwatch_log_group" "api_lambda_cloudwatch" {
  name = "/aws/api_gw/${aws_apigatewayv2_api.lambda_api.name}"

  retention_in_days = 1
}

resource "aws_lambda_permission" "api_perm_f1" {
  statement_id  = var.lambda_perm_statement_id
  action        = var.lambda_perm_action
  function_name = aws_lambda_function.function_1.function_name
  principal     = var.lambda_perm_principal

  source_arn = "${aws_apigatewayv2_api.lambda_api.execution_arn}/*"
}

resource "aws_lambda_permission" "api_perm_f2" {
  statement_id  = var.lambda_perm_statement_id
  action        = var.lambda_perm_action
  function_name = aws_lambda_function.function_2.function_name
  principal     = var.lambda_perm_principal

  source_arn = "${aws_apigatewayv2_api.lambda_api.execution_arn}/*"
}

resource "aws_lambda_permission" "api_perm_f3" {
  statement_id  = var.lambda_perm_statement_id
  action        = var.lambda_perm_action
  function_name = aws_lambda_function.function_3.function_name
  principal     = var.lambda_perm_principal

  source_arn = "${aws_apigatewayv2_api.lambda_api.execution_arn}/*"
}